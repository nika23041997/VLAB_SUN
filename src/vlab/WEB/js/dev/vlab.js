function init_lab() {
    const byDefault = {
        Planet1: "Меркурий",
        Planet2: "Юпитер",
        Planet3: "Венера",
        rad1: 2440,
        rad2: 69911,
        rad3: 6052
    };

    let answers = {
        answer1: 0,
        answer2: 0,
        answer3: 0
    };

    //let planets=['Меркурий','Нептун','Венера','Земля','Марс','Юпитер']

    function parseVariant(str, def_obj) {
        let parse_str;
        if (typeof str === 'string' && str !== "") {
            try {
                parse_str = JSON.parse(str);
            }
            catch (e) {
                if (def_obj){
                    parse_str = def_obj;
                }
                else {
                    parse_str = false;
                }
            }
        }
        else {
            if (def_obj){
                parse_str = def_obj;
            }
            else {
                parse_str = false;
            }
        }
        return parse_str;
    }

    return {
        setVariant : function(str){
            let variant;
            if (str !== undefined) {
                variant = parseVariant(str, byDefault);
            }
            else {
                variant = byDefault;
            }
            return variant;
        },
        setPreviousSolution: function(str){
            let previousSolution;
            if (str !== undefined) {
                try {
                    previousSolution = JSON.parse(str);
                }
                catch (e) {}
            }
            return previousSolution;
        },


        init: function () {
            let variant = this.setVariant($("#preGeneratedCode").val());
            let previousSolution = this.setPreviousSolution($("#previousSolution").val());
            if (previousSolution !== undefined) {
                answers.answer1 = previousSolution.answer1;
                answers.answer2 = previousSolution.answer2;
                answers.answer3 = previousSolution.answer3;
            }
            else {
                answers.answer1 = [];
                answers.answer2 = [];
                answers.answer3 = [];
            }
            let content = '' +
                '<div class = "header">' +
                '<h1>Планеты</h1>' +
                '<input type="button" class="btn btn-info" id = "infoModalOpener" value = "Справка">' +
                '</div>' +
                '<div class = "lab-initial">' +
                '<h2>Исходные данные</h2>' +
                '<ul class="list-group">' +
                '<li class="">Planet1: ' + variant.Planet1+ '</li>' +
                '<li class="">Planet2: ' + variant.Planet2 + '</li>' +
                '<li class="">Planet3: ' + variant.Planet3+ '</li>' +
                '</ul>' +
                '</div>' +
                '<div class = "lab-task">' +
                '<h2>Найти плотность при соответствующих планетам радиусах</h2>' +
                '<ol class="list-group">' +
                '<li>' + variant.rad1 + '</li>' +
                '<li>' + variant.rad2 + '</li>' +
                '<li>' + variant.rad3 + '</li>' +
                '</ol>' +
                '</div>'+
                '<div class = "lab-task">'+
                '<button type="button" id = "button" class="btn btn-success btn-rounded">Построить график</button>'+
                '</div>' +
                '<div class = "lab-answers">' +
                '<h2>Ответ</h2>' +
                '<ul class="list-group">' +
                '<li>' +
                ' <input type = "text" id = "answer1"  style="width: 150px;">' +
                '</li>' +
                '<li>' +
                '<input type = "text" id = "answer2"  style="width: 150px;">' +
                '</li>' +
                '<li>' +
                '<input type = "text" id = "answer3"  style="width: 150px;">' +
                '</li>' +
                '</ul>' +
                '</div>' +
                '<div class = "info">' +
                '<h2>Виртуальная лаборатория "Планеты солнечной системы"</h2>' +
                '<p>Для выполнения задания виртуальной лаборатории необходимо найти значения плотности планет воспользовавшись исходным радиусом и значением свободного падения. Гравитационная постоянная: G=6*10^(-11)</p>' +
                '</div>'+
                '<div class ="graph" id="myDiv">'+
                '</div>';
            $("#jsLab").html(content);

            $(".info").css("display", "none");
            $("#answer1").on("input", function() {
                answers.answer1 = $("#answer1").val();
            });
            $("#answer2").on("input", function() {
                answers.answer2 = $("#answer2").val();
            });
            $("#answer3").on("input", function() {
                answers.answer3 = $("#answer3").val();
            });

            $("#infoModalOpener").on("click", function () {
                if ($(".info").css("display").toLowerCase() == "none") {
                    $(".info").css("display", "block");
                    this.value = "Закрыть";
                    $(".lab-initial").css("display", "none");
                    $(".lab-task").css("display", "none");
                    $(".lab-answers").css("display", "none");
                    $(".graph").css("display", "none");
                }
                else {
                    $(".info").css("display", "none");
                    this.value = "Справка";
                    $(".lab-initial").css("display", "block");
                    $(".lab-task").css("display", "block");
                    $(".lab-answers").css("display", "block");
                    $(".graph").css("display", "block");
                }
            });

            $("#button").on("click", function () {
                var trace2 = {
                    x: [answers.answer1, answers.answer2, answers.answer3],
                    y: [variant.rad1, variant.rad2,variant.rad3],
                    mode: 'markers',
                    type: 'scatter',
                    text: [variant.Planet1, variant.Planet2,variant.Planet3],
                    marker: { size: 12 }
                };

                var data = [ trace2 ];

                var layout = {
                    xaxis: {
                        range: [ 0, 10000 ]
                    },
                    yaxis: {
                        range: [0, 70000]
                    },
                    title:'Graph'
                };


                $(".graph").css("display", "block");
                Plotly.newPlot("myDiv", data, layout);


            });

        },
        calculateHandler: function (text, code) {},
        getResults: function () {
            return JSON.stringify(answers);
        },
        getCondition: function () {}
    };
}

var Vlab = init_lab();