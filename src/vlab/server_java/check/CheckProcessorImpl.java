package vlab.server_java.check;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Arrays;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONArray;
import rlcp.check.ConditionForChecking;
import rlcp.generate.GeneratingResult;
import rlcp.server.processor.check.CheckProcessor;
import rlcp.server.processor.check.PreCheckProcessor;
import rlcp.server.processor.check.PreCheckProcessor.PreCheckResult;
import rlcp.server.processor.check.PreCheckResultAwareCheckProcessor;


public class CheckProcessorImpl implements PreCheckResultAwareCheckProcessor<String> {
    private static int getResult(int radius, String pl) {
        final double G = 0.0000000000667;
        int result;
        double g=0;
        switch(pl) {
            case "Меркурий":
                g = 3.7;
                break;
            case "Нептун":
                g = 11.2;
                break;
            case "Венера":
                g = 8.9;
                break;
            case "Земля":
                g = 9.78;
                break;
            case "Марс":
                g=3.76;
                break;
            case "Юпитер":
                g=23.50;
                break;
            case "Сатурн":
                g=9.06;
                break;
            case "Уран":
                g=9.8;
                break;
            case "Луна":
                g=1.62;
                break;
        }
        result = (int) ((3*g)/(4*G*Math.PI*(radius*1000)));
        return result;
    }

    @Override
    public CheckingSingleConditionResult checkSingleCondition(ConditionForChecking condition, String instructions, GeneratingResult generatingResult) throws Exception {
        BigDecimal points=new BigDecimal(0.0);
        String comment = "";

        try {
            JSONObject variant_json = new JSONObject(generatingResult.getCode());
            String A = variant_json.getString("Planet1");
            String B = variant_json.getString("Planet2");
            String C = variant_json.getString("Planet3");
            Integer expr1 = variant_json.getInt("rad1");
            Integer expr2 = variant_json.getInt("rad2");
            Integer expr3 = variant_json.getInt("rad3");


            JSONObject answers_json = new JSONObject(instructions);
            int answer1 = 0;
            int answer2 = 0;
            int answer3 = 0;
            try {
                answer1 = answers_json.getInt("answer1");
            } catch (Exception e) {
                comment = "Введите целое число";
            }
            try {
                answer2 = answers_json.getInt("answer2");
            } catch (Exception e) {
                comment = "Введите целое число";
            }
            try {
                answer3 = answers_json.getInt("answer3");
            } catch (Exception e) {
                comment = "Введите целое число";
            }

            double trueAnswer1 = getResult(expr1, A);
            double trueAnswer2 = getResult(expr2, B);
            double trueAnswer3 = getResult(expr3, C);

            double trueAnswers = 0.0;
            if (answer1 != 0 && answer2 != 0 && answer3 != 0) {
                if (answer1 == trueAnswer1) {
                    trueAnswers += 1.0;
                } else {
                    comment += "Неверное значение плотности. Ваш ответ для планеты " + A + " " + answer1 + ". Ожидался ответ " + trueAnswer1 + ". ";
                }
                if (answer2 == trueAnswer2) {
                    trueAnswers += 1.0;
                } else {
                    comment += "Неверное значение плотности. Ваш ответ для планеты " + B + " " + answer2 + ". Ожидался ответ " + trueAnswer2 + ". ";
                }
                if (answer3 == trueAnswer3) {
                    trueAnswers += 1.0;
                } else {
                    comment += "Неверное значение плотности. Ваш ответ для планеты " + C + " " + answer3 + ". Ожидался ответ " + trueAnswer3 + ".";
                }

                points = new BigDecimal(Math.round((trueAnswers / 3.0) * 100.0) / 100.0);
                if (points.compareTo(new BigDecimal("1.0")) == 0) {
                    comment = "Решение верно";
                }
            }
        }
        catch(Exception e) {
            points = new BigDecimal(0.0);
            comment = "Server problem, " + e.getMessage();
            e.printStackTrace();
        }

        return new CheckingSingleConditionResult(points, comment);
    }

    @Override
    public void setPreCheckResult(PreCheckResult<String> preCheckResult) {}
}

