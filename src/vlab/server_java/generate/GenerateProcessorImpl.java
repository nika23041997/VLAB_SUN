package vlab.server_java.generate;
import org.json.JSONObject;
import java.util.*;
import rlcp.generate.GeneratingResult;
import rlcp.server.processor.generate.GenerateProcessor;

public class GenerateProcessorImpl implements GenerateProcessor {
    Map<String,Integer> planets = new HashMap<String,Integer>();
    private void setPlanet() {
        planets.put("Меркурий",2440);
        planets.put("Нептун",25000);
        planets.put("Венера",6052);
        planets.put("Земля",6371);
        planets.put("Марс",3396);
        planets.put("Юпитер",69911);
        planets.put("Сатурн",58252);
        planets.put("Уран",25560);
        planets.put("Луна",1740);
        
    }
    private Map<String,Integer> getPlanet() {
        List<String> keysList = new ArrayList<String>(planets.keySet());
        Map<String,Integer> temp = new HashMap<String,Integer>();
        int max = keysList.size()-1;
        int min = 0;
        Random rand = new Random();
        int randomIndex = rand.nextInt(max - min + 1) + min;
        int randomIndex2 = rand.nextInt(max - min + 1) + min;   //nextInt(keysList.size());
        int randomIndex3 = rand.nextInt(max - min + 1) + min;
        String randomKey = keysList.get(randomIndex);
        String randomKey2 = keysList.get(randomIndex2);
        String randomKey3 = keysList.get(randomIndex3);
        temp.put(randomKey,planets.get(randomKey));
        temp.put(randomKey2,planets.get(randomKey2));
        temp.put(randomKey3,planets.get(randomKey3));
        return temp;
    }

    @Override
    public GeneratingResult generate(String condition) {
        String text = "";
        String code = "";
        String instructions = "";
        setPlanet();
        Map<String,Integer> temp ;
        temp=getPlanet();
        List<String> keyListPL = new ArrayList<String>(planets.keySet());
        List<String> keyListA = new ArrayList<String>(temp.keySet());
        JSONObject variant = new JSONObject();
        String A = keyListA.get(0);
        String B = keyListA.get(1);
        String C="";
        try{
            C= keyListA.get(2);
        }
        catch (Exception e){
            for (int i=0;i< keyListPL.size();i++){
                if (!keyListPL.get(i).equals(A) && !keyListPL.get(i).equals(B)){
                    C=keyListPL.get(i);
                }
            }
        }


        variant.put("Planet1", A);
        variant.put("Planet2", B);
        variant.put("Planet3", C);

        Integer rad1 = planets.get(A);
        Integer rad2 = planets.get(B);
        Integer rad3 = planets.get(C);

        variant.put("rad1",rad1);
        variant.put("rad2", rad2);
        variant.put("rad3", rad3);

        text = "Ваш вариант загружен в установку";
        code = variant.toString();
        instructions = "";

        return new GeneratingResult(text, code, instructions);
    }
}
